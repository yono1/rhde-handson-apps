import os
from flask import Flask, Response
from kafka import KafkaConsumer
import numpy as np
import cv2

# 環境変数
KAFKA_TOPIC = "video" if (os.environ.get("KAFKA_TOPIC") is None) else os.environ.get("KAFKA_TOPIC")
KAFKA_BOOTSTRAP_SERVER = "localhost:9092" if (os.environ.get("KAFKA_BOOTSTRAP_SERVER") is None) else os.environ.get("KAFKA_BOOTSTRAP_SERVER")
PORT = 3000 if (os.environ.get("PORT") is None) else os.environ.get("PORT")

consumer = KafkaConsumer(
    KAFKA_TOPIC,
    bootstrap_servers=[KAFKA_BOOTSTRAP_SERVER])


# Set the consumer in a Flask App
app = Flask(__name__)

@app.route('/', methods=['GET'])
def video():
    return Response(
        get_video_stream(),
        mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/test', methods=['GET'])
def test():
    return "Hello Container World!!"

# KAFKA_TOPICから画像データをconsumeして描画
def get_video_stream():

    msg = []
    for msg in consumer:
        input_img = np.frombuffer(msg.value, dtype=np.uint8)

        # decode msg
        input_img = cv2.imdecode(input_img, -1)

        _, buffer = cv2.imencode('.jpg', input_img)

        yield (b'--frame\r\n'
               b'Content-Type: image/jpg\r\n\r\n' + buffer.tobytes() + b'\r\n\r\n') 


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=int(PORT))
