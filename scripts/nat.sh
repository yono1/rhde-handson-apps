#!/bin/bash

if [ "$1" = "rhde" ]; then

   # Update the following variables to fit your setup
   GUEST_IP=$3
   GUEST_PORT=$4
   HOST_PORT=$5

   if [ "$2" = "delete" ] ; then
    /sbin/iptables -D FORWARD -o virbr0 -d  $GUEST_IP -j ACCEPT
    /sbin/iptables -t nat -D PREROUTING -p tcp --dport $HOST_PORT -j DNAT --to $GUEST_IP:$GUEST_PORT
   fi
   if [ "$2" = "add" ] ; then
    /sbin/iptables -I FORWARD -o virbr0 -d  $GUEST_IP -j ACCEPT
    /sbin/iptables -t nat -I PREROUTING -p tcp --dport $HOST_PORT -j DNAT --to $GUEST_IP:$GUEST_PORT
   fi
fi