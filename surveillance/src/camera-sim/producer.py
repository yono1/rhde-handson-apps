import os
import cv2
# import pafy
import time
import datetime
from kafka import KafkaProducer

# 環境変数
KAFKA_TOPIC = "video" if (os.environ.get("KAFKA_TOPIC") is None) else os.environ.get("KAFKA_TOPIC")
KAFKA_BOOTSTRAP_SERVER = "localhost:9092" if (os.environ.get("KAFKA_BOOTSTRAP_SERVER") is None) else os.environ.get("KAFKA_BOOTSTRAP_SERVER")
VIDEO_URL = "sample.mp4" if (os.environ.get("VIDEO_URL") is None) else os.environ.get("VIDEO_URL")
FRAME_INTERVAL = 0.03 if (os.environ.get("FRAME_INTERVAL") is None) else os.environ.get("FRAME_INTERVAL")
WIDTH = 160 if (os.environ.get("WIDTH") is None) else os.environ.get("WIDTH")
HEIGHT = 120 if (os.environ.get("HEIGHT") is None) else os.environ.get("HEIGHT")

# Youtubenの動画を取得する準備
# video = pafy.new(VIDEO_URL)
# best = video.getbest(preftype="mp4")
# print(datetime.datetime.now(), ": prepare to read video")

def publish_camera():
    # Start up producer
    producer = KafkaProducer(bootstrap_servers=[KAFKA_BOOTSTRAP_SERVER])
    print(datetime.datetime.now(), ": Connected successfully to kafka cluster")

    # Youtube動画をキャプチャ
    camera = cv2.VideoCapture(VIDEO_URL)
    camera.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('H', '2', '6', '4'));
    camera.set(cv2.CAP_PROP_BUFFERSIZE, 1);
    camera.set(cv2.CAP_PROP_FRAME_WIDTH, int(WIDTH))
    camera.set(cv2.CAP_PROP_FRAME_HEIGHT, int(HEIGHT))

    while(True):
        _, frame = camera.read()
        print(datetime.datetime.now(), ": read camera capture")
        _, buffer = cv2.imencode('.jpg', frame,(cv2.IMWRITE_JPEG_QUALITY, 90))
        print(datetime.datetime.now(), ": encoding")
        producer.send(KAFKA_TOPIC, buffer.tobytes())
        print(datetime.datetime.now(), ": send the frame")

        # reduced load on processor
        time.sleep(float(FRAME_INTERVAL))

if __name__ == '__main__':
    publish_camera()
